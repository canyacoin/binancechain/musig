package musigservice

import (
	"fmt"

	sdk "github.com/cosmos/cosmos-sdk/types"
)

// NewHandler returns a handler for "musigservice" type messages.
func NewHandler(keeper Keeper) sdk.Handler {
	return func(ctx sdk.Context, msg sdk.Msg) sdk.Result {
		switch msg := msg.(type) {
		case MsgSetSigner:
			return handleMsgSetSigner(ctx, keeper, msg)
		case MsgGenMuSig:
			return handleMsgGenMuSig(ctx, keeper, msg)
		default:
			errMsg := fmt.Sprintf("Unrecognized musigservice Msg type: %v", msg.Type())
			return sdk.ErrUnknownRequest(errMsg).Result()
		}
	}
}

// Handle a message to set musigname
func handleMsgSetSigner(ctx sdk.Context, keeper Keeper, msg MsgSetSigner) sdk.Result {
	if !msg.Owner.Equals(keeper.GetOwner(ctx, msg.MuSigName)) { // Checks if the the msg sender is the same as the current owner
		return sdk.ErrUnauthorized("Incorrect Owner").Result() // If not, throw an error
	}
	keeper.SetSigner(ctx, msg.MuSigName, msg.SignerKey) // If so, set the musigname to the walletaddr specified in the msg.
	return sdk.Result{}                      // return
}

// Handle a message to generate musig
func handleMsgGenMuSig(ctx sdk.Context, keeper Keeper, msg MsgGenMuSig) sdk.Result {
	if keeper.GetFee(ctx, msg.MuSigName).IsAllGT(msg.Fee) { // Checks if the the fee fee is greater than the fee paid by the current owner
		return sdk.ErrInsufficientCoins("Fee not high enough").Result() // If not, throw an error
	}
	if keeper.HasOwner(ctx, msg.MuSigName) {
		_, err := keeper.coinKeeper.SendCoins(ctx, msg.Creator, keeper.GetOwner(ctx, msg.MuSigName), msg.Fee)
		if err != nil {
			return sdk.ErrInsufficientCoins("Creator does not have enough coins").Result()
		}
	} else {
		_, _, err := keeper.coinKeeper.SubtractCoins(ctx, msg.Creator, msg.Fee) // If so, deduct the Fee amount from the sender
		if err != nil {
			return sdk.ErrInsufficientCoins("Creator does not have enough coins").Result()
		}
	}
	keeper.SetSignerKey(ctx, msg.MuSigName, msg.SignerKey)
	keeper.SetMValue(ctx, msg.MuSigName, msg.MValue)
	keeper.SetNValue(ctx, msg.MuSigName, msg.NValue)
	keeper.SetOwner(ctx, msg.MuSigName, msg.Creator)
	keeper.SetFee(ctx, msg.MuSigName, msg.Fee)
	return sdk.Result{}
}
