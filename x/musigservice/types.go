package musigservice

import (
	sdk "github.com/cosmos/cosmos-sdk/types"
)

// Initial Starting Fee for a musigname that was never previously owned
var MinNameFee = sdk.Coins{sdk.NewInt64Coin("mstoken", 1)}

// MuSig is a struct that contains all the metadata of a musigname
type MuSig struct {
	WalletAddr sdk.AccAddress     `json:"walletaddr"`
	SignerKey string     `json:"signerkey"`
	MValue string         `json:"mvalue"`
	NValue string         `json:"nvalue"`
	Owner sdk.AccAddress  `json:"owner"`
	Fee sdk.Coins       `json:"fee"`
}

// Returns a new MuSig with the minfee as the fee
func NewMuSig() MuSig {
	return MuSig{
		Fee: MinNameFee,
	}
}
