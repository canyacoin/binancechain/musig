package cli

import (
	"fmt"

	"github.com/cosmos/cosmos-sdk/client/context"
	"github.com/cosmos/cosmos-sdk/codec"
	"github.com/canyacoin/binance-musig/x/musigservice"
	"github.com/spf13/cobra"
)

// GetCmdResolveName queries information about a musigname
func GetCmdResolveMuSigName(queryRoute string, cdc *codec.Codec) *cobra.Command {
	return &cobra.Command{
		Use:   "resolve [musigname]",
		Short: "resolve musigname",
		Args:  cobra.ExactArgs(1),
		RunE: func(cmd *cobra.Command, args []string) error {
			cliCtx := context.NewCLIContext().WithCodec(cdc)
			musigname := args[0]

			res, err := cliCtx.QueryWithData(fmt.Sprintf("custom/%s/resolve/%s", queryRoute, musigname), nil)
			if err != nil {
				fmt.Printf("could not resolve musigname - %s \n", string(musigname))
				return nil
			}

			var out musigservice.QueryResResolve
			cdc.MustUnmarshalJSON(res, &out)
			return cliCtx.PrintOutput(out)
		},
	}
}

// GetCmdMuSig queries information about a domain
func GetCmdMuSig(queryRoute string, cdc *codec.Codec) *cobra.Command {
	return &cobra.Command{
		Use:   "musig [musigname]",
		Short: "Query musig info of musigname",
		Args:  cobra.ExactArgs(1),
		RunE: func(cmd *cobra.Command, args []string) error {
			cliCtx := context.NewCLIContext().WithCodec(cdc)
			musigname := args[0]

			res, err := cliCtx.QueryWithData(fmt.Sprintf("custom/%s/musig/%s", queryRoute, musigname), nil)
			if err != nil {
				fmt.Printf("could not resolve musig - %s \n", string(musigname))
				return nil
			}

			var out musigservice.MuSig
			cdc.MustUnmarshalJSON(res, &out)
			return cliCtx.PrintOutput(out)
		},
	}
}

// GetCmdNames queries a list of all musigids
func GetCmdMuSigNames(queryRoute string, cdc *codec.Codec) *cobra.Command {
	return &cobra.Command{
		Use:   "musignames",
		Short: "musignames",
		// Args:  cobra.ExactArgs(1),
		RunE: func(cmd *cobra.Command, args []string) error {
			cliCtx := context.NewCLIContext().WithCodec(cdc)

			res, err := cliCtx.QueryWithData(fmt.Sprintf("custom/%s/musignames", queryRoute), nil)
			if err != nil {
				fmt.Printf("could not get query musignames\n")
				return nil
			}

			var out musigservice.QueryResMuSigNames
			cdc.MustUnmarshalJSON(res, &out)
			return cliCtx.PrintOutput(out)
		},
	}
}
