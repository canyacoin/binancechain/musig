package client

import (
	"github.com/cosmos/cosmos-sdk/client"
	musigservicecmd "github.com/canyacoin/binance-musig/x/musigservice/client/cli"
	"github.com/spf13/cobra"
	amino "github.com/tendermint/go-amino"
)

// ModuleClient exports all client functionality from this module
type ModuleClient struct {
	storeKey string
	cdc      *amino.Codec
}

func NewModuleClient(storeKey string, cdc *amino.Codec) ModuleClient {
	return ModuleClient{storeKey, cdc}
}

// GetQueryCmd returns the cli query commands for this module
func (mc ModuleClient) GetQueryCmd() *cobra.Command {
	// Group musigservice queries under a subcommand
	musigserviceQueryCmd := &cobra.Command{
		Use:   "musigservice",
		Short: "Querying commands for the musigservice module",
	}

	musigserviceQueryCmd.AddCommand(client.GetCommands(
		musigservicecmd.GetCmdResolveMuSigName(mc.storeKey, mc.cdc),
		musigservicecmd.GetCmdMuSig(mc.storeKey, mc.cdc),
		musigservicecmd.GetCmdMuSigNames(mc.storeKey, mc.cdc),
	)...)

	return musigserviceQueryCmd
}

// GetTxCmd returns the transaction commands for this module
func (mc ModuleClient) GetTxCmd() *cobra.Command {
	musigserviceTxCmd := &cobra.Command{
		Use:   "musigservice",
		Short: "musigservice transactions subcommands",
	}

	musigserviceTxCmd.AddCommand(client.PostCommands(
		musigservicecmd.GetCmdGenMuSig(mc.cdc),
		musigservicecmd.GetCmdSetSigner(mc.cdc),
	)...)

	return musigserviceTxCmd
}
