package musigservice

import (
	"github.com/cosmos/cosmos-sdk/codec"
)

// RegisterCodec registers concrete types on the Amino codec
func RegisterCodec(cdc *codec.Codec) {
	cdc.RegisterConcrete(MsgSetSigner{}, "musigservice/SetSigner", nil)
	cdc.RegisterConcrete(MsgGenMuSig{}, "musigservice/GenMuSig", nil)
}
