package musigservice

import (
	"github.com/cosmos/cosmos-sdk/codec"
	"github.com/cosmos/cosmos-sdk/x/bank"

	sdk "github.com/cosmos/cosmos-sdk/types"
)

// Keeper maintains the link to data storage and exposes getter/setter methods for the various parts of the state machine
type Keeper struct {
	coinKeeper bank.Keeper

	storeKey sdk.StoreKey // Unexposed key to access store from sdk.Context

	cdc *codec.Codec // The wire codec for binary encoding/decoding.
}

// NewKeeper creates new instances of the musigservice Keeper
func NewKeeper(coinKeeper bank.Keeper, storeKey sdk.StoreKey, cdc *codec.Codec) Keeper {
	return Keeper{
		coinKeeper: coinKeeper,
		storeKey:   storeKey,
		cdc:        cdc,
	}
}

// Gets the entire MuSig metadata struct for a musigname
func (k Keeper) GetMuSig(ctx sdk.Context, musigname string) MuSig {
	store := ctx.KVStore(k.storeKey)
	if !store.Has([]byte(musigname)) {
		return NewMuSig()
	}
	bz := store.Get([]byte(musigname))
	var musig MuSig
	k.cdc.MustUnmarshalBinaryBare(bz, &musig)
	return musig
}

// Sets the entire MuSig metadata struct for a musigname
func (k Keeper) SetMuSig(ctx sdk.Context, musigname string, musig MuSig) {
	if musig.Owner.Empty() {
		return
	}
	store := ctx.KVStore(k.storeKey)
	store.Set([]byte(musigname), k.cdc.MustMarshalBinaryBare(musig))
}

// ResolveName - returns the string that the musigname resolves to
func (k Keeper) ResolveName(ctx sdk.Context, musigname string) sdk.AccAddress {
	return k.GetMuSig(ctx, musigname).WalletAddr
}

// SetSigner - sets the walletaddr string that a musigname resolves to
func (k Keeper) SetSigner(ctx sdk.Context, musigname string, signerkey string) {
	musig := k.GetMuSig(ctx, musigname)
	musig.SignerKey = signerkey
	k.SetMuSig(ctx, musigname, musig)
}

// HasOwner - returns whether or not the musigname already has an owner
func (k Keeper) HasOwner(ctx sdk.Context, musigname string) bool {
	return !k.GetMuSig(ctx, musigname).Owner.Empty()
}

// GetOwner - get the current owner of a musigname
func (k Keeper) GetOwner(ctx sdk.Context, musigname string) sdk.AccAddress {
	return k.GetMuSig(ctx, musigname).Owner
}

// SetOwner - sets the current owner of a musigname
func (k Keeper) SetOwner(ctx sdk.Context, musigname string, owner sdk.AccAddress) {
	musig := k.GetMuSig(ctx, musigname)
	musig.Owner = owner
	k.SetMuSig(ctx, musigname, musig)
}

// GetFee - gets the current fee of a musigname
func (k Keeper) GetFee(ctx sdk.Context, musigname string) sdk.Coins {
	return k.GetMuSig(ctx, musigname).Fee
}

// SetFee - sets the current fee of a musigname
func (k Keeper) SetFee(ctx sdk.Context, musigname string, fee sdk.Coins) {
	musig := k.GetMuSig(ctx, musigname)
	musig.Fee = fee
	k.SetMuSig(ctx, musigname, musig)
}

// GetMValue - gets the threshold of a musigname
func (k Keeper) GetMValue(ctx sdk.Context, musigname string) string {
	return k.GetMuSig(ctx, musigname).MValue
}

// SetMValue - sets the threshold of a musigname
func (k Keeper) SetMValue(ctx sdk.Context, musigname string, mvalue string) {
	musig := k.GetMuSig(ctx, musigname)
	musig.MValue = mvalue
	k.SetMuSig(ctx, musigname, musig)
}

// GetNValue - gets the number of a musigname
func (k Keeper) GetNValue(ctx sdk.Context, musigname string) string {
	return k.GetMuSig(ctx, musigname).NValue
}

// SetNValue- sets the number of a musigname
func (k Keeper) SetNValue(ctx sdk.Context, musigname string, nvalue string) {
	musig := k.GetMuSig(ctx, musigname)
	musig.NValue = nvalue
	k.SetMuSig(ctx, musigname, musig)
}

// GetSignerKey - gets the SignerKey of a musigname
func (k Keeper) GetSignerKey(ctx sdk.Context, musigname string) string {
	return k.GetMuSig(ctx, musigname).SignerKey
}

// SetSignerKey - sets the SignerKey of a musigname
func (k Keeper) SetSignerKey(ctx sdk.Context, musigname string, signerkey string) {
	musig := k.GetMuSig(ctx, musigname)
	musig.SignerKey = signerkey
	k.SetMuSig(ctx, musigname, musig)
}

// GetWalletAddr - gets the WalletAddr of a musigname
func (k Keeper) GetWalletAddr(ctx sdk.Context, musigname string) sdk.AccAddress {
	return k.GetMuSig(ctx, musigname).WalletAddr
}

// SetWalletAddr - sets the WalletAddr of a musigname
func (k Keeper) SetWalletAddr(ctx sdk.Context, musigname string, walletaddr sdk.AccAddress) {
	musig := k.GetMuSig(ctx, musigname)
	musig.WalletAddr = walletaddr
	k.SetMuSig(ctx, musigname, musig)
}




// Get an iterator over all musigids in which the keys are the musigids and the values are the musig
func (k Keeper) GetNamesIterator(ctx sdk.Context) sdk.Iterator {
	store := ctx.KVStore(k.storeKey)
	return sdk.KVStorePrefixIterator(store, nil)
}
