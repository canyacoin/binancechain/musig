package musigservice

import (
	"encoding/json"

	sdk "github.com/cosmos/cosmos-sdk/types"
)

// MsgSetSigner defines a SetSigner message
type MsgSetSigner struct {
	MuSigName  string
	SignerKey string
	Owner sdk.AccAddress
}

// NewMsgSetSigner is a constructor function for MsgSetSigner
func NewMsgSetSigner(musigname string, signerkey string, owner sdk.AccAddress) MsgSetSigner {
	return MsgSetSigner{
		MuSigName:  musigname,
		SignerKey: signerkey,
		Owner: owner,
	}
}

// Route should return the musigname of the module
func (msg MsgSetSigner) Route() string { return "musigservice" }

// Type should return the action
func (msg MsgSetSigner) Type() string { return "set_signer" }

// ValidateBasic runs stateless checks on the message
func (msg MsgSetSigner) ValidateBasic() sdk.Error {
	if msg.Owner.Empty() {
		return sdk.ErrInvalidAddress(msg.Owner.String())
	}
	if len(msg.MuSigName) == 0 || len(msg.SignerKey) == 0 {
		return sdk.ErrUnknownRequest("MuSigName and/or SignerKey cannot be empty")
	}
	return nil
}

// GetSignBytes encodes the message for signing
func (msg MsgSetSigner) GetSignBytes() []byte {
	b, err := json.Marshal(msg)
	if err != nil {
		panic(err)
	}
	return sdk.MustSortJSON(b)
}

// GetSigners defines whose signature is required
func (msg MsgSetSigner) GetSigners() []sdk.AccAddress {
	return []sdk.AccAddress{msg.Owner}
}

// MsgGenMuSig defines the GenMuSig message
type MsgGenMuSig struct {
	MuSigName  string
	SignerKey  string
	MValue string
	NValue string
	Fee   sdk.Coins
	Creator sdk.AccAddress
}

// NewMsgGenMuSig is the constructor function for MsgGenMuSig
func NewMsgGenMuSig(musigname string, signerkey string, mvalue string, nvalue string, fee sdk.Coins, creator sdk.AccAddress) MsgGenMuSig {
	return MsgGenMuSig{
		MuSigName:  musigname,
		SignerKey: signerkey,
		MValue:  mvalue,
		NValue:  nvalue,
		Fee:   fee,
		Creator: creator,
	}
}

// Route should return the musigname of the module
func (msg MsgGenMuSig) Route() string { return "musigservice" }

// Type should return the action
func (msg MsgGenMuSig) Type() string { return "gen_musig" }

// ValidateBasic runs stateless checks on the message
func (msg MsgGenMuSig) ValidateBasic() sdk.Error {
	if msg.Creator.Empty() {
		return sdk.ErrInvalidAddress(msg.Creator.String())
	}
	if len(msg.MuSigName) == 0 {
		return sdk.ErrUnknownRequest("MuSigName cannot be empty")
	}
	if !msg.Fee.IsAllPositive() {
		return sdk.ErrInsufficientCoins("Fee must be positive")
	}
	return nil
}

// GetSignBytes encodes the message for signing
func (msg MsgGenMuSig) GetSignBytes() []byte {
	b, err := json.Marshal(msg)
	if err != nil {
		panic(err)
	}
	return sdk.MustSortJSON(b)
}

// GetSigners defines whose signature is required
func (msg MsgGenMuSig) GetSigners() []sdk.AccAddress {
	return []sdk.AccAddress{msg.Creator}
}
