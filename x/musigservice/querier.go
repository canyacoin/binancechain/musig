package musigservice

import (
	"fmt"
	"strings"

	"github.com/cosmos/cosmos-sdk/codec"

	sdk "github.com/cosmos/cosmos-sdk/types"
	abci "github.com/tendermint/tendermint/abci/types"
)

// query endpoints supported by the musigservice Querier
const (
	QueryResolve = "resolve"
	QueryMuSig   = "musig"
	QueryNames   = "musignames"
)

// NewQuerier is the module level router for state queries
func NewQuerier(keeper Keeper) sdk.Querier {
	return func(ctx sdk.Context, path []string, req abci.RequestQuery) (res []byte, err sdk.Error) {
		switch path[0] {
		case QueryResolve:
			return queryResolve(ctx, path[1:], req, keeper)
		case QueryMuSig:
			return queryMuSig(ctx, path[1:], req, keeper)
		case QueryNames:
			return queryMusigNames(ctx, req, keeper)
		default:
			return nil, sdk.ErrUnknownRequest("unknown musigservice query endpoint")
		}
	}
}

// nolint: unparam
func queryResolve(ctx sdk.Context, path []string, req abci.RequestQuery, keeper Keeper) (res []byte, err sdk.Error) {
	musigname := path[0]

	walletaddr := keeper.ResolveName(ctx, musigname)

	if walletaddr == nil {
		return []byte{}, sdk.ErrUnknownRequest("could not resolve musigname")
	}

	bz, err2 := codec.MarshalJSONIndent(keeper.cdc, QueryResResolve{walletaddr})
	if err2 != nil {
		panic("could not marshal result to JSON")
	}

	return bz, nil
}

// Query Result Payload for a resolve query
type QueryResResolve struct {
	WalletAddr sdk.AccAddress `json:"walletaddr"`
}

// implement fmt.Stringer
func (r QueryResResolve) String() string {
	return strings.TrimSpace(fmt.Sprintf(
		`WalletAddr: %s`, r.WalletAddr))
}

// nolint: unparam
func queryMuSig(ctx sdk.Context, path []string, req abci.RequestQuery, keeper Keeper) (res []byte, err sdk.Error) {
	musigname := path[0]

	musig := keeper.GetMuSig(ctx, musigname)

	bz, err2 := codec.MarshalJSONIndent(keeper.cdc, musig)
	if err2 != nil {
		panic("could not marshal result to JSON")
	}

	return bz, nil
}

// implement fmt.Stringer
func (w MuSig) String() string {
	return strings.TrimSpace(fmt.Sprintf(
		`WalletAddr: %s
SignerKey: %s
MValue: %s
NValue: %s
Fee: %s
Owner: %s`, w.WalletAddr, w.SignerKey, w.MValue, w.NValue, w.Fee, w.Owner))
}

func queryMusigNames(ctx sdk.Context, req abci.RequestQuery, keeper Keeper) (res []byte, err sdk.Error) {
	var musigNamesList QueryResMuSigNames

	iterator := keeper.GetNamesIterator(ctx)

	for ; iterator.Valid(); iterator.Next() {
		musigname := string(iterator.Key())
		musigNamesList = append(musigNamesList, musigname)
	}

	bz, err2 := codec.MarshalJSONIndent(keeper.cdc, musigNamesList)
	if err2 != nil {
		panic("could not marshal result to JSON")
	}

	return bz, nil
}

// Query Result Payload for a musigids query
type QueryResMuSigNames []string

// implement fmt.Stringer
func (n QueryResMuSigNames) String() string {
	return strings.Join(n[:], "\n")
}
